const sqs = require('ya-sqs');

describe('sqs', function describe() {
    this.timeout(10000);

    it('should be able to connect', async () => {
        const sqsUrl = process.env.AWS_SQS_SERVICE_URL || 'http://localhost:4576';

        console.log(`SQS service url: ${sqsUrl}`);


        const q = await sqs.createQueue({
            aws: {
                region: 'eu-central-1',
                accessKeyId: 'NotNeededForLocalStack',
                secretAccessKey: 'hush',
                sqs: {
                    endpoint: sqsUrl
                }
            },
            name: 'queue_of_doom'
        });

        await q.push({
            whoHasTwoHornsAndOneTail: 'the devil'
        });
    });
});